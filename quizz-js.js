// On déclare les variables
let submitBtn = document.querySelector('#submit-btn');
let themeChoice = document.querySelectorAll('input');
let formSection = document.querySelector('#questions');
let firstContent = document.querySelector('.form-group');
let categoryChoice;

// Level choice : add a listener to submit button:
submitBtn.addEventListener('click', function () {
    //to know the user's choice:
    for (let i = 0; i < themeChoice.length; i++) {
        if (themeChoice[i].checked) {
            categoryChoice = themeChoice[i].value;
            console.log(categoryChoice);
        }
    }

    // First we hide the previous question:
    firstContent.style.display = 'none';

    if (categoryChoice === "1") {
        //Then create the new elements:
        //1st, Form group <div class="form-group">
        let newForm1 = document.createElement('div');
        newForm1.classList.add('form-group');

        formSection.appendChild(newForm1);

        //2nd, the question <p class="text-center">
        let newQuest1 = document.createElement('p');
        newQuest1.classList.add('text-center');
        newQuest1.textContent = 'Les plaques d\'immatriculations ont été inventées à Lyon ?!'; // Had contents in the question

        newForm1.appendChild(newQuest1);

        // 3rd, the div Row where are the answers <div class="row w-100 d-flex justify-content-center allign-items-center m-0 p-0">
        let newDivRow1 = document.createElement('div');
        newDivRow1.classList.add('row', 'w-100', 'd-flex', 'justify-content-center', 'align-items-center', 'm-2', 'p-0');

        newQuest1.appendChild(newDivRow1);

        // Then the div for each answer <div class="form-check form-check-inline">
        let newDivAnswer1 = document.createElement('div');
        newDivAnswer1.classList.add('form-check', 'form-check-inline');

        newDivRow1.appendChild(newDivAnswer1);

        //The input <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1">
        let newInput1 = document.createElement('input');
        newInput1.classList.add('form-check-input');
        newInput1.type = 'radio';
        newInput1.id = 'inLineRadio1';
        newInput1.setAttribute('name', 'inlineRadioOptions');

        newDivAnswer1.appendChild(newInput1);

        // True <label class="form-check-label" for="inlineRadio1">1</label>
        let newLabel1 = document.createElement('label');
        newLabel1.classList.add('form-check-label');
        newLabel1.setAttribute('for', 'inLineRadio1');
        newLabel1.textContent = 'Vrai';
        newDivAnswer1.appendChild(newLabel1);


        // Then the div for 2nd answer <div class="form-check form-check-inline">
        let newDivAnswer2 = document.createElement('div');
        newDivAnswer2.classList.add('form-check', 'form-check-inline');

        newDivRow1.appendChild(newDivAnswer2);


        //The input for 2nd answer <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1">
        let newInput2 = document.createElement('input');
        newInput2.classList.add('form-check-input');
        newInput2.type = 'radio';
        newInput2.id = 'inLineRadio2';
        newInput2.setAttribute('name', 'inlineRadioOptions');

        newDivAnswer2.appendChild(newInput2);

        // False <label class="form-check-label" for="inlineRadio1">1</label>
        let newLabel2 = document.createElement('label');
        newLabel2.classList.add('form-check-label');
        newLabel2.setAttribute('for', 'inLineRadio2');
        newLabel2.textContent = 'Faux';
        newDivAnswer2.appendChild(newLabel2);

        // Add function for listening to the answer
        let firstAnswer = document.querySelectorAll('.input');
        submitBtn.addEventListener('click', function () {

            for (let i = 0; i < firstAnswer.length ; i++){
                console.log(firstAnswer);
            }
        }); // Close click function on the submit button for Ast question of the first choice
    }//Close if(choice === "1")
}); // Close click function on the submit button for themeChoice



// FACTORISATION DE CE BORDEL : On peut mettre uniquement la ligne qui change dans la condition if..else puis déclarer tout le reste ensuite ..